# Cable Management
3D printable cable management trays.  They are designed to hang off the back of
your desk (in my case, a fairly standard piece of Ikea furniture) and support
not just cables, but small pieces of electrical equipment - small
computers, routers, power bricks, that sort of thing.  More info on 'why'
[is here](https://snowgoons.ro/posts/2020-09-05-zen-and-the-art-of-cable-management/).

### Author
Copyright (c) 2020 by Tim Walls- https://snowgoons.ro/

Licensed under a CC BY-NC-SA license - see LICENSE.md for details.


## What's in the box
There are three components included.  I've included sliced gcodes as well
as my own project files from Cura, but you will want to start with the STL
files and slice them for your own 3D Printer.  The three files (in the
```bodies``` directory) you care about are:

| File        | Description |
| ----------- | ----------- |
| cable-tray.stl | The 'heavy' cable tray, designed for a pair to be able to safely support a 2018 Mac Mini as well as associated cables. |
| light-cable-tray.stl | A lighter-weight cable tray, smaller and intended to support just cables or small electrical devices.  The sizing permits a pair to support a TP-Link 8-port switch. |
| grubscrew.stl | If you are hanging lightweight components or indeed just cables, you'll want the tray to grip the desk (for heavier items, it's designed for gravity to do the job.)  These grubscrews screw into a corresponding hold in the trays to clamp the desk from below. |

### Using the heavy tray to support a computer
THe heavier cable tray in ```cable-tray.stl``` is designed to support my
Mac Mini, or any similarly sized computer.  The design is based on an intended
load of around 7.5 Newtons each, so two of them should be able to support a
weight of around 1.7kg.  Static load simulation in ```studies/Intended Maximum Load.html```
shows the level of deflection expected under this load, with generic Plastic
as the simulation material.

Simulating a 3D Printed part is obviously inexact, but when
printed with a *50% infill density*, the deflection seen in practice matches
that predicted by the simulation (see ```pictures/actual-deflection.jpg```).

Your mileage may vary, of course, depending on your printer settings and
material - so, printer beware, but it works well for me!

### Other assembly notes
The screwhole and grubscrew are M12 threaded; this is modelled but is  at the margins for
printability, however I didn't want to make them any larger because the
region around the screwhole is the weakest point under load, and making it
larger probably means making the whole thing bigger and heavier.

However, I recommend a regular M12 thread tapping set - using the thread tapper
on the grubscrew and thread hole you can easily get a perfect fit between
the two.

## Printing Tips
There are no tricky overhangs or the like, everything should print easily.
My settings are:

* Wall thickness: 0.8mm
* Adhesion: Brim (this is mostly just my personal preference)
* Infill:
  * cable-tray.stl - Gyroid, 50%
  * others - Gyroid, 15%
* Supports: No
* Print rate: 60mm/s
* Material: Colorfabb PLA/PHA


